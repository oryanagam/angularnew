import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  private path:string = 'https://firebasestorage.googleapis.com/v0/b/hello-jce11.appspot.com/o/'; 
  public images:string[] = [];
  constructor() {
    this.images[0] = this.path + 'biz.JPG' + '?alt=media&token=ff313690-21ed-4f9c-861d-1f15d86b9846';
    this.images[1] = this.path + 'entermnt.JPG' + '?alt=media&token=b4e1ebff-211d-4865-a519-766832a9c1b4';
    this.images[2] = this.path + 'politics-icon.png' + '?alt=media&token=8952d778-6e25-4ce9-be38-bc127285b1de';
    this.images[3] = this.path + 'sport.JPG' + '?alt=media&token=50470f18-6f36-49d1-99f2-e1da2ab6c770'; 
    this.images[4] = this.path + 'tech.JPG' + '?alt=media&token=d425e6ae-e087-4b07-b5dd-60b9a84173e5';
   }
}
