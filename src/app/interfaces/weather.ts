export interface Weather {
    name:string,
    country:string,
    image:string,
    description:string,
    temperature:number,
    lan?:number,
    lon?:number
}
