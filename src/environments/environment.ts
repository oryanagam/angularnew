// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyAh5NbdAsY4mDRVYZF37Og1w9WfBrIim1w",
    authDomain: "hello-jce11.firebaseapp.com",
    projectId: "hello-jce11",
    storageBucket: "hello-jce11.appspot.com",
    messagingSenderId: "490906913635",
    appId: "1:490906913635:web:3b7979529638ee5b9d2156"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
